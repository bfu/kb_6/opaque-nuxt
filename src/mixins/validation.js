export const MIN_LENGTH_PASSWORD = 8

export const requiredRuleMixin = {
  /* eslint-disable */
  data: function () {
    return {
      rules: {
        required: v => !!v || this.$t('services.validation.required')
      }
    }
  }
}

export const requiredArrayRuleMixin = {
  /* eslint-disable */
  data: function () {
    return {
      rules: {
        requiredArray: v => v.length || this.$t('services.validation.required')
      }
    }
  }
}

export const emailRuleMixin = {
  data: function () {
    return {
      rules: {
        email: v => !v || /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v)
          || this.$t('services.validation.email')
      }
    }
  }
}

export const passwordRuleMixin = {
  data: function () {
    return {
      rules: {
        password: v => v.length >= MIN_LENGTH_PASSWORD
          || this.$t('services.validation.min-length', {length: MIN_LENGTH_PASSWORD}),
        password_confirm: v => this.model.form.password === this.model.form.password_confirm
          || this.$t('services.validation.password-confirm'),
      }
    }
  }
}


