import { SodiumService } from '@/services/SodiumService'
import { OprfService } from './OprfService'

let sodium = null

export class RegisterService extends SodiumService {
  static URL = '/api/auth/register'

  static async start({ email, password }) {
    /**
     * Client OprfSk: r
     * alpha: hashToPoint(password) * r
     */
    // eslint-disable-next-line
    const { r, hash, point, maskedPoint: alpha } = await OprfService.start(password)

    const encodedPoint = OprfService.encodePoint(alpha)

    return { email, oprfSk: r, hash, point, challenge: encodedPoint }
  }

  static async finish(password, oprfSk, challenge, serverOprfPk, serverPk) {
    sodium = await this.getSodium()

    serverPk = OprfService.decodePoint(serverPk)

    serverOprfPk = OprfService.decodePoint(serverOprfPk)

    const rwd = await OprfService.finish(
      password,
      challenge,
      oprfSk,
      serverOprfPk
    )

    // apply argon2 to rwd using the hardening params sent from the server
    const key = sodium.crypto_pwhash(
      sodium.crypto_secretbox_KEYBYTES,
      rwd,
      Buffer.alloc(sodium.crypto_pwhash_SALTBYTES),
      sodium.crypto_pwhash_OPSLIMIT_INTERACTIVE,
      sodium.crypto_pwhash_MEMLIMIT_INTERACTIVE,
      sodium.crypto_pwhash_ALG_DEFAULT
    )

    const { publicKey: pk, privateKey: sk } = sodium.crypto_kx_keypair()

    const params = Buffer.from(
      JSON.stringify({
        userPk: pk,
        userSk: sk,
        serverPk,
      })
    )

    const nonce = sodium.randombytes_buf(sodium.crypto_secretbox_NONCEBYTES)

    const ciphertext = sodium.crypto_secretbox_easy(params, nonce, key)

    return { key, nonce, ciphertext, pk, sk }
  }
}
