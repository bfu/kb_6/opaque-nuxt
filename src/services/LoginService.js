import { SodiumService } from '@/services/SodiumService'
import { OprfService } from './OprfService'

/**
 * 1. Send email to server
 * 2. Generate keypair
 * 3. Perform OPRF flow with password as input
 * 4. Encrypt keypair and server public key using OPRF finish as key
 * 5. Send encrypted parameters and public key to server
 */

export class LoginService extends SodiumService {
  static URL = '/api/auth/login'

  static async start({ email, password }, oprfSk) {
    /**
     * Client OprfSk: r
     * alpha: hashToPoint(password) * r
     */
    const { r, hash, point, maskedPoint: alpha } = await OprfService.start(
      password,
      oprfSk
    )

    const encodedPoint = OprfService.encodePoint(alpha)

    return { email, hash, point, challenge: encodedPoint, oprfSk: r }
  }

  static async finish(password, oprfSk, envelope, challenge, serverOprfPk) {
    const sodium = await this.getSodium()

    serverOprfPk = OprfService.decodePoint(serverOprfPk)

    const rwd = await OprfService.finish(
      password,
      challenge,
      oprfSk,
      serverOprfPk
    )

    // apply argon2 to rwd using the hardening params sent from the server
    const key = sodium.crypto_pwhash(
      sodium.crypto_secretbox_KEYBYTES,
      rwd,
      Buffer.alloc(sodium.crypto_pwhash_SALTBYTES),
      sodium.crypto_pwhash_OPSLIMIT_INTERACTIVE,
      sodium.crypto_pwhash_MEMLIMIT_INTERACTIVE,
      sodium.crypto_pwhash_ALG_DEFAULT
    )

    let { ciphertext, nonce } = JSON.parse(envelope)

    nonce = new Uint8Array(Object.values(nonce))

    ciphertext = new Uint8Array(Object.values(ciphertext))

    let openedEnvelope = sodium.crypto_secretbox_open_easy(
      ciphertext,
      nonce,
      key
    )

    openedEnvelope = Buffer.from(openedEnvelope).toString()

    // eslint-disable-next-line
    let { userPk, userSk, serverPk } = JSON.parse(openedEnvelope)

    userPk = new Uint8Array(Object.values(userPk))

    userSk = new Uint8Array(Object.values(userSk))

    serverPk = new Uint8Array(Object.values(serverPk))

    console.time('keys')

    const {
      sharedRx: decryptKey,
      sharedTx: encryptKey,
    } = sodium.crypto_kx_client_session_keys(userPk, userSk, serverPk)

    console.timeEnd('keys')

    return { encryptKey, decryptKey, userPk, userSk, serverPk, key }
  }
}
