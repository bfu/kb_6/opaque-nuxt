export const _sodium = require('libsodium-wrappers-sumo')

export class SodiumService {
  static async getSodium() {
    await _sodium.ready

    return _sodium
  }
}
