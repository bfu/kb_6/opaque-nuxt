export default function ({ $axios, app, redirect }) {
  $axios.onRequest((config) => {
    config.headers['I18N-Lang'] = app.i18n.locale

    if (process.client) {
      config.headers.Authorization = localStorage.getItem('token')
    }
  })

  $axios.onError((error) => {
    if (error.response.status === 401) {
      app.store.dispatch('auth/logout')

      redirect('/' + app.i18n.locale)
    }
  })
}
