const isDev = process.env.NODE_ENV !== 'production'

export default {
  ...(!isDev && {
    modern: 'client',
  }),

  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s',
    title: 'OpaqueNuxt',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'OpaqueNuxt',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      {
        src:
          'https://polyfill.io/v2/polyfill.min.js?features=IntersectionObserver',
      },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: false,
  /*
   ** Global CSS
   */
  css: ['assets/main.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/axios.js'],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    'cookie-universal-nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    [
      'nuxt-i18n',
      {
        locales: [
          {
            code: 'en',
            name: 'English',
            icon: '/lang/en.svg',
            file: 'en.json',
          },
          {
            code: 'ru',
            name: 'Русский',
            icon: '/lang/ru.svg',
            file: 'ru.json',
          },
        ],
        defaultLocale: 'en',
        lazy: true,
        langDir: 'lang/',
        detectBrowserLanguage: {
          useCookie: true,
          alwaysRedirect: true,
        },
      },
    ],
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true, // Can be also an object with default options
    debug: isDev,
    credentials: true,
    init(axios) {
      axios.defaults.withCredentials = true
    },
    // port: process.env.BASE_PORT,
  },

  proxy: {
    // Simple proxy
    '/api': process.env.API_URL,
  },

  i18n: {
    vueI18n: '~/plugins/vue-i18n.js',
  },

  router: {
    // middleware: 'auth'
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      light: true,
    },
    css: true,
    treeShake: true,
  },
  /*
   ** Build configuration
   */
  build: {
    ...(!isDev && {
      html: {
        minify: {
          collapseBooleanAttributes: true,
          decodeEntities: true,
          minifyCSS: true,
          minifyJS: true,
          processConditionalComments: true,
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          trimCustomFragments: true,
          useShortDoctype: true,
        },
      },
    }),
    splitChunks: {
      layouts: true,
      pages: true,
      commons: true,
    },
    optimization: {
      minimize: !isDev,
    },
    ...(!isDev && {
      extractCSS: {
        ignoreOrder: true,
      },
    }),
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
}
